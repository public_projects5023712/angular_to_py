import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 400 || error.status === 404 )  {
          console.log(error);
          Swal.fire({
            title: 'Error!',
            text: error.error.error == null ? error.error.message : error.error.error,
            icon: 'error',
            confirmButtonText: 'Aceptar',
            timer: 3000
          });
        }else if(error.status == 401){
          console.log(error);

          Swal.fire({
            title: 'Unauthorized!',
            text: error.error.error == null ? error.error.message : error.error.error,
            icon: 'info',
            timer: 3000,
            confirmButtonText: 'Aceptar'
          });
        }else if(error.status == 500){
          Swal.fire({
            title: 'Error!',
            text: 'Ocurrio un error',
            icon: 'error',
            timer: 3000,
            confirmButtonText: 'Aceptar'
          });
        }
        return throwError(() => error);
      })
    );
  }
}
