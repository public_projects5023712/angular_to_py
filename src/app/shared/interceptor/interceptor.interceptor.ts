import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token');

    if(token !== null) {
      req = req.clone({
        setHeaders: {
          'Authorization': `Bearer ${token}`,
        },
      });
      return next.handle(req);
    }else{
      req = req.clone({
        setHeaders: {
          'Authorization': `Bearer`,
        },
      });
      return next.handle(req);
    }

  }

}
