import { Component, HostListener, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ThemeOptions } from '../../../theme-options';
import { select } from '@angular-redux/store';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {

	permissions: any;
	userData: any;

	constructor(
		public globals: ThemeOptions,
		private router: Router,
	) {}

	@select('config') public config$: Observable<any>;

	private newInnerWidth: number;
	private innerWidth: number;

	toggleSidebar() {
		this.globals.toggleSidebar = !this.globals.toggleSidebar;
	}

	sidebarHover() {
		this.globals.sidebarHover = !this.globals.sidebarHover;
	}

	ngOnInit() {

		setTimeout(() => {
			this.innerWidth = window.innerWidth;
			if (this.innerWidth < 1200) {
				this.globals.toggleSidebar = true;
			}
		});

	}

	verifyActiveMenu = (route: string) => (this.router.url.includes(route)) ? route : '';

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.newInnerWidth = event.target.innerWidth;

		if (this.newInnerWidth < 1200) {
			this.globals.toggleSidebar = true;
		} else {
			this.globals.toggleSidebar = false;
		}

	}

}
