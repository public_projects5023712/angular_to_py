export const DEPENDENCES_API = {
    'mediatorsType': {
        'natural': 1,
        'legal': 2
    },
    'typeIdentification': {
        'cc': 1,
        'cc-ext': 2,
        'nit': 3,
        'unknown': 4
    }
};