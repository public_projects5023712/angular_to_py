import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
//sidebar
import { SidebarComponent } from './Layout/Components/sidebar/sidebar.component';
import { LogoComponent } from './Layout/Components/sidebar/elements/logo/logo.component';
//page-title
import { PageTitleComponent } from './Layout/Components/page-title/page-title.component';
//header
import { HeaderComponent } from './Layout/Components/header/header.component';
import { DotsComponent } from './Layout/Components/header/elements/dots/dots.component';
import { DrawerComponent } from './Layout/Components/header/elements/drawer/drawer.component';
import { MegamenuComponent } from './Layout/Components/header/elements/mega-menu/mega-menu.component';
import { SearchBoxComponent } from './Layout/Components/header/elements/search-box/search-box.component';
import { UserBoxComponent } from './Layout/Components/header/elements/user-box/user-box.component';
//base-layout
import { BaseLayoutComponent } from './Layout/base-layout/base-layout.component';
//themeOptions
import { LoadingBarModule } from '@ngx-loading-bar/core';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [
    SidebarComponent,
    LogoComponent,
    PageTitleComponent,
    HeaderComponent,
    DotsComponent,
    DrawerComponent,
    MegamenuComponent,
    SearchBoxComponent,
    UserBoxComponent,
    BaseLayoutComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    LoadingBarModule,
    PerfectScrollbarModule,
    NgbModule,

  ],
  exports : [
    SidebarComponent,
    LogoComponent,
    PageTitleComponent,
    HeaderComponent,
    DotsComponent,
    DrawerComponent,
    MegamenuComponent,
    SearchBoxComponent,
    UserBoxComponent,
    BaseLayoutComponent,
  ]
})
export class SharedModule {}
