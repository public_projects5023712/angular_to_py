import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ProductService {

	constructor(private http: HttpClient) {}

  get(){
		return this.http.get(`${environment.apiURL}product`).pipe(map((response: any) => {
			return response;
		}));
	}

}
