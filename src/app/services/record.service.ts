import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class RecordService {

	constructor(private http: HttpClient) {}

	create(data){
		return this.http.post(`${environment.apiURL}record`,data).pipe(map((response: any) => {
			return response;
		}));
	}

  get(){
		return this.http.get(`${environment.apiURL}record`).pipe(map((response: any) => {
			return response;
		}));
	}

  updateSend(id){
		return this.http.put(`${environment.apiURL}record?id=${id}`,id).pipe(map((response: any) => {
			return response;
		}));
	}

}
