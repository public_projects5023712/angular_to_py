import { CheckLoginGuard } from './shared/guards/check-login.guard';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagesModule } from './pages/pages.module';
import {NgReduxModule} from '@angular-redux/store';
import { SharedModule } from './shared/shared.module';
import {ConfigActions} from './shared/ThemeOptions/store/config.actions';
import {ThemeOptions} from './shared/theme-options';
//perfecScrobar
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {rootReducer, ArchitectUIState} from '../app/shared/ThemeOptions/store';
import {NgRedux, DevToolsExtension} from '@angular-redux/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InterceptorInterceptor } from './shared/interceptor/interceptor.interceptor';
import { HttpErrorInterceptor } from './shared/interceptor/error.interceptor';
import { LoaderInterceptor } from './shared/interceptor/loader.interceptor';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PagesModule,
    NgReduxModule,
    SharedModule,
    PerfectScrollbarModule,
    NgbModule,
    FontAwesomeModule,
    SlickCarouselModule
  ],
  providers: [
    {
      provide:
      PERFECT_SCROLLBAR_CONFIG,
      // DROPZONE_CONFIG,
      useValue:
      DEFAULT_PERFECT_SCROLLBAR_CONFIG,
      // DEFAULT_DROPZONE_CONFIG,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },/*
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },*/
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorInterceptor,
      multi: true
    },
    ConfigActions,
    ThemeOptions,
    CheckLoginGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private ngRedux: NgRedux<ArchitectUIState>,
    private devTool: DevToolsExtension) {

    this.ngRedux.configureStore(
    rootReducer,
    {} as ArchitectUIState,
    [],
    [devTool.isEnabled() ? devTool.enhancer() : f => f]
    );

    }
}
