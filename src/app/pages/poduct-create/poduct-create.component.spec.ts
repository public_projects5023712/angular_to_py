import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoductCreateComponent } from './poduct-create.component';

describe('PoductCreateComponent', () => {
  let component: PoductCreateComponent;
  let fixture: ComponentFixture<PoductCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoductCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoductCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
