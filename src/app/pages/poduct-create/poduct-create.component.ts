import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { RecordService } from 'src/app/services/record.service';
import { ProductService } from './../../services/product.service';

@Component({
  selector: 'app-poduct-create',
  templateUrl: './poduct-create.component.html',
  styleUrls: ['./poduct-create.component.scss']
})
export class PoductCreateComponent implements OnInit {
  slideConfig2 = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '0',
    slidesToShow: 1,
    speed: 500,
    dots: true,
  };

  productForm: FormGroup;
	returnUrl: string;
	error: string;
  productList;

  constructor(
    private router: Router,
		private formBuilder: FormBuilder,
    private productService: ProductService,
    private recordService: RecordService
  ) { }

  ngOnInit(): void {
    this.getList();
    this.setFormGroup();
  }


  setFormGroup()
  {
    this.productForm = this.formBuilder.group({
			name: ['', Validators.required],
			product_id: ['', Validators.required],
      serie: ['', Validators.required],
			activate_at: ['', Validators.required],
		});
  }

  getList(){
    this.productService.get().subscribe(
      response => {
        this.productList = response;
      },
			(error: any) => this.error = error
		);
	}

  create(){
    console.log('data',this.productForm.value);

    this.recordService.create(this.productForm.value).subscribe(
      response => {
        this.productForm.reset();
        Swal.fire({
          title: 'Completo',
          text: 'Registo guardado con éxito, ¿Quieres ir a listado de inventario?',
          icon: 'success',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Sí, ir ahora!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/podruct-list']);
          }
        });
      },
			(error: any) => this.error = error
		);
	}

}
