import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { RecordService } from 'src/app/services/record.service';

@Component({
  selector: 'app-poduct-list',
  templateUrl: './poduct-list.component.html',
  styleUrls: ['./poduct-list.component.scss']
})
export class PoductListComponent implements OnInit {
  slideConfig2 = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '0',
    slidesToShow: 1,
    speed: 500,
    dots: true,
  };

  productForm: FormGroup;
	returnUrl: string;
	error: string;
  recordList;

  constructor(
		private formBuilder: FormBuilder,
    private recordService: RecordService
  ) { }

  ngOnInit(): void {
    this.getList();
    this.setFormGroup();
  }


  setFormGroup()
  {
    this.productForm = this.formBuilder.group({
			name: ['', Validators.required],
			product_id: ['', Validators.required],
      serie: ['', Validators.required],
			activate_at: ['', Validators.required],
		});
  }

  getList(){
    this.recordService.get().subscribe(
      response => {
        this.recordList = response;
      },
			(error: any) => this.error = error
		);
	}

  send(id){
    this.recordService.updateSend(id).subscribe(
      response => {
        this.getList();
        Swal.fire({
          title: 'Completo',
          text: 'Registo entregado con éxito!',
          icon: 'success',
          confirmButtonText: 'Aceptar',
          timer: 5000
        });
      },
			(error: any) => this.error = error
		);
	}

}
