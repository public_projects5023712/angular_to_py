import { PoductCreateComponent } from './poduct-create/poduct-create.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PoductListComponent } from './poduct-list/poduct-list.component';

const routes: Routes = [
  { path: '',  component: DashboardComponent,
   children: [
    { path: '',  component:PoductCreateComponent , data: {extraParameter: ''}},
    { path: 'podruct-list',  component:PoductListComponent , data: {extraParameter: ''}},
    ]},
  { path: 'users/user-create',  component: DashboardComponent , data: {extraParameter: ''}},
  { path: 'power-bi',  component: DashboardComponent , data: {extraParameter: ''}}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
